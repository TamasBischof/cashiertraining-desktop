﻿using CashierTraining.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CashierTraining
{
    /// <summary>
    /// EventArgs implementation for main menu button clicks.
    /// </summary>
    public class MainMenuButtonEventArgs : RoutedEventArgs
    {
        public readonly IEnumerable<ITrainingEntryProvider> Entries;

        public MainMenuButtonEventArgs(RoutedEvent routedEvent, IEnumerable<ITrainingEntryProvider> entryProviders) : base(routedEvent)
        {
            Entries = entryProviders;
        }
    }
}
