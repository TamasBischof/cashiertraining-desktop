﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CashierTraining
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : UserControl
    {
        internal MainMenuViewModel viewModel;

        public static readonly RoutedEvent TrainButtonClickedEvent = EventManager.RegisterRoutedEvent(
            "TrainButtonClicked", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(MainMenu));
        public static readonly RoutedEvent LearnButtonClickedEvent = EventManager.RegisterRoutedEvent(
            "LearnButtonClicked", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(MainMenu));

        public event RoutedEventHandler TrainButtonClicked
        {
            add { AddHandler(TrainButtonClickedEvent, value); }
            remove { RemoveHandler(TrainButtonClickedEvent, value); }
        }

        public event RoutedEventHandler LearnButtonClicked
        {
            add { AddHandler(TrainButtonClickedEvent, value); }
            remove { RemoveHandler(LearnButtonClickedEvent, value); }
        }

        public MainMenu()
        {
            InitializeComponent();
            viewModel = new MainMenuViewModel();
            languagePicker.ItemsSource = ((App)Application.Current).SupportedCultures;
            languagePicker.SelectedItem = ((App)Application.Current).SupportedCultures.Find(x => x.Equals(System.Threading.Thread.CurrentThread.CurrentUICulture));
            toolTipLabel.Text = Properties.Resources.Help;
        }

        private void LearnButton_Click(object sender, RoutedEventArgs e)
        {
            var eventArgs = new MainMenuButtonEventArgs(LearnButtonClickedEvent,viewModel.EntryProviders.Where(x => x.Selected));
            RaiseEvent(eventArgs);
        }

        private void TrainButton_Click(object sender, RoutedEventArgs e)
        {
            var eventArgs = new MainMenuButtonEventArgs(TrainButtonClickedEvent, viewModel.EntryProviders.Where(x => x.Selected));
            RaiseEvent(eventArgs);
        }

        private void LanguagePicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CultureInfo info = languagePicker.SelectedItem as CultureInfo;
            ChangeCulture(info);
        }

        //have to refresh all bindings
        internal void ChangeCulture(CultureInfo info)
        {
            Properties.Resources.Culture = info;
            System.Threading.Thread.CurrentThread.CurrentCulture = info;
            System.Threading.Thread.CurrentThread.CurrentUICulture = info;
            trainButton.Content = Properties.Resources.TrainingButtonLabel;
            learnButton.Content = Properties.Resources.LearningButtonLabel;
            manageFileButton.Content = Properties.Resources.ManageFileButton;
            toolTipLabel.Text = Properties.Resources.Help;
        }

        private void LearnButton_MouseEnter(object sender, MouseEventArgs e)
        {
            toolTipLabel.Text = Properties.Resources.LearningButtonToolTip;
        }

        private void TrainButton_MouseEnter(object sender, MouseEventArgs e)
        {
            toolTipLabel.Text = Properties.Resources.TrainingButtonToolTip;
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            toolTipLabel.Text = Properties.Resources.Help;
        }

        private void ManageFileButton_Click(object sender, RoutedEventArgs e)
        {
            var window = new FileManageWindow(viewModel);
            window.Show();
        }
    }
}
