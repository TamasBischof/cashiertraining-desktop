﻿using CashierTraining.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CashierTraining
{
    /// <summary>
    /// Interaction logic for FileManageWindow.xaml
    /// </summary>
    public partial class FileManageWindow : Window
    {
        public MainMenuViewModel ViewModel { get; private set; }
        EntryProviderFactory factory = new EntryProviderFactory();

        public FileManageWindow(MainMenuViewModel viewModel)
        {
            ViewModel = viewModel;
            InitializeComponent();
            this.DataContext = this;
        }

        private void FileList_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                CopyFiles(files);
                
            }
        }

        private void CopyFiles(string[] fileNames)
        {
            foreach (var fileName in fileNames)
            {
                try
                {
                    File.Copy(fileName, Path.Combine(Path.GetFullPath(@"CSV"), Path.GetFileName(fileName)));
                }
                catch { continue; }
            }
        }
    }
}
