﻿using CashierTraining.Data;
using CashierTraining.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CashierTraining
{
    /// <summary>
    /// Bridge between view and data on a TrainingUserControl.
    /// </summary>
    public class TrainingViewModel : INotifyPropertyChanged
    {
        InputEvaluator evaluator = new InputEvaluator();
        ScoreTracker scoreTracker;
        TrainingSet data;

        public event PropertyChangedEventHandler PropertyChanged;

        private string evaluationText;
        public string EvaluationText
        {
            get
            {
                return evaluationText;
            }
            private set
            {
                evaluationText = value;
                OnPropertyChanged();
            }
        }

        //expose some of the trainingset's properties for data binding
        public TrainingEntry Current => data.Current;
        public string CurrentInput => evaluator.CurrentInput;
        //same for scoretracker
        public string Score => scoreTracker.Score;
        public string Failed => $"{Properties.Resources.ErrorCount}: {scoreTracker.Failed}";

        public EventHandler<RoutedEventArgs> Finished;

        public TrainingViewModel(IEnumerable<ITrainingEntryProvider> entryProviders)
        {
            data = new TrainingSet(entryProviders);
            scoreTracker = new ScoreTracker(data.Count);
            evaluator.InputComplete += Evaluator_InputComplete;
            evaluator.SetExpectedInput(Current.ProductCode); //set the first entry
            Application.Current.MainWindow.PreviewKeyDown += MainWindow_KeyDown; //get all keypresses from the application
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key >= Key.D0 && e.Key <= Key.D9)
            {
                char input = char.Parse(e.Key.ToString().TrimStart('D'));
                evaluator.AddInput(input);
                OnPropertyChanged(nameof(CurrentInput));
            }
            else if (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)
            {
                char input = char.Parse(e.Key.ToString().Replace("NumPad", ""));
                evaluator.AddInput(input);
                OnPropertyChanged(nameof(CurrentInput));
            }
        }

        private async void Evaluator_InputComplete(object sender, InputEvaluatorEventArgs e)
        {
            EvaluationText = e.Success ? $"{Properties.Resources.Success}" : $"{Properties.Resources.Failure}, {Properties.Resources.ExpectedCode}: {evaluator.ExpectedInput}";

            await Task.Delay(1500); //small delay before the next exercise

            SetNewEntry(e.Success);
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void SetNewEntry(bool success) //pass whether the user has guessed the answer correctly
        {
            data.AdvanceTraining(success, scoreTracker);
            OnPropertyChanged(nameof(Current));

            EvaluationText = "";

            evaluator.SetExpectedInput(Current.ProductCode);
            OnPropertyChanged(nameof(CurrentInput));
            OnPropertyChanged(nameof(Score));
            OnPropertyChanged(nameof(Failed));
            if (data.Current == TrainingEntry.Null) Finish();
        }

        private void Finish()
        {
            Application.Current.MainWindow.PreviewKeyDown -= MainWindow_KeyDown;
            var summary = new SummaryWindow(SummaryType.Training, scoreTracker.Score, scoreTracker.ScoreOnSecond, scoreTracker.Failed);
            summary.Show();
            Finished?.Invoke(this, null);
        }
    }
}
