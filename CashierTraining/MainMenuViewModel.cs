﻿using CashierTraining.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace CashierTraining
{
    /// <summary>
    /// Bridge between view and logic for a MainMenuUserControl.
    /// </summary>
    public class MainMenuViewModel
    {
        public ObservableCollection<ITrainingEntryProvider> EntryProviders { get; private set; }
        public EventHandler ProvidersChanged { get; internal set; }
        EntryProviderFactory factory = new EntryProviderFactory();
        FileSystemWatcher watcher;

        public MainMenuViewModel()
        {
            EntryProviders = new ObservableCollection<ITrainingEntryProvider>(factory.GetProviders());

            watcher = new FileSystemWatcher(Path.GetFullPath(@"CSV"));
            watcher.Filter = "*.csv";
            watcher.EnableRaisingEvents = true;
            watcher.Created += FileAdded;
            watcher.Deleted += FileDeleted;
        }

        private void FileAdded(object sender, FileSystemEventArgs e)
        {
            App.Current.Dispatcher.Invoke((Action)delegate 
            {
                EntryProviders.Add(factory.GetCSVProvider(e.FullPath));
            });
        }

        private void FileDeleted(object sender, FileSystemEventArgs e)
        {
            var provider = EntryProviders.Where(x => e.Name == x.DisplayName).SingleOrDefault();
            if (provider != null)
            {
               App.Current.Dispatcher.Invoke((Action)delegate
               {
                   EntryProviders.Remove(provider);
               });
            }
        }
    }
}
