﻿using System.Windows;

namespace CashierTraining
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MainMenu mainMenu = new MainMenu();

        public MainWindow()
        {
            InitializeComponent();
            contentHolder.Content = mainMenu;
        }

        private void HandleTrainButtonClick(object sender, RoutedEventArgs e)
        {
            MainMenuButtonEventArgs args = e as MainMenuButtonEventArgs;
            contentHolder.Content = new TrainingUserControl(args.Entries);
        }

        private void HandleLearnButtonClick(object sender, RoutedEventArgs e)
        {
            MainMenuButtonEventArgs args = e as MainMenuButtonEventArgs;
            contentHolder.Content = new LearningUserControl(args.Entries);
        }

        private void HandleQuitButtonClick(object sender, RoutedEventArgs e)
        {
            contentHolder.Content = mainMenu;
        }
    }
}
