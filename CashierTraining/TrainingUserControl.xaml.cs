﻿using CashierTraining.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CashierTraining
{
    /// <summary>
    /// Interaction logic for TrainingUserControl.xaml
    /// </summary>
    public partial class TrainingUserControl : UserControl
    {
        public TrainingUserControl(IEnumerable<ITrainingEntryProvider> entryProviders)
        {
            InitializeComponent();
            var viewModel = new TrainingViewModel(entryProviders);
            DataContext = viewModel;
            viewModel.Finished += QuitButton_Click;
        }

        public static readonly RoutedEvent QuitButtonClickedEvent = EventManager.RegisterRoutedEvent(
            "QuitButtonClicked", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(TrainingUserControl));

        public event RoutedEventHandler QuitButtonClicked
        {
            add { AddHandler(QuitButtonClickedEvent, value); }
            remove { RemoveHandler(QuitButtonClickedEvent, value); }
        }

        private void QuitButton_Click(object sender, RoutedEventArgs e)
        {
            var eventArgs = new RoutedEventArgs(QuitButtonClickedEvent);
            RaiseEvent(eventArgs);
        }
    }
}
