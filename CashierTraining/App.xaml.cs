﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Configuration;
using System.Globalization;

namespace CashierTraining
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        List<CultureInfo> supportedCultures = new List<CultureInfo> { new CultureInfo("en"), new CultureInfo("hu-HU"), new CultureInfo("de-DE")};

        public List<CultureInfo> SupportedCultures { get => supportedCultures; }

        protected override void OnStartup(StartupEventArgs e)
        {
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["Culture"]))
            {
                CultureInfo currentCulture = CultureInfo.CurrentCulture;
                if (SupportedCultures.Contains(currentCulture))
                {
                    ConfigurationManager.AppSettings["Culture"] = currentCulture.Name;
                }
                else
                {
                    ConfigurationManager.AppSettings["Culture"] = "en";
                }
            }

            CultureInfo info = new CultureInfo(ConfigurationManager.AppSettings["Culture"]);
            System.Threading.Thread.CurrentThread.CurrentCulture = info;
            System.Threading.Thread.CurrentThread.CurrentUICulture = info;
            base.OnStartup(e);
        }
    }
}
