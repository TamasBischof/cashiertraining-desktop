﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CashierTraining
{
    public enum SummaryType { Training, Learning }

    public partial class SummaryWindow : Window
    {
        public SummaryWindow(SummaryType summaryType, string firstTry, string secondTry, string incorrect)
        {
            InitializeComponent();
            if (summaryType == SummaryType.Learning)
            {
                Title = Properties.Resources.SummaryLearningTitle;
                titleText.Text = Properties.Resources.LearningComplete;
                secondSuccessText.Text = "";
            }
            else if (summaryType == SummaryType.Training)
            {
                Title = Properties.Resources.SummaryTrainingTitle;
                titleText.Text = Properties.Resources.TrainingComplete;
                secondSuccessText.Text = $"{Properties.Resources.SummarySecondTryLabel}: {secondTry}";
            }
            firstSuccessText.Text = $"{Properties.Resources.SummaryCorrectLabel}: {firstTry}";
            failureText.Text = $"{Properties.Resources.SummaryFailedLabel}: {incorrect}";
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
