﻿using System;

namespace CashierTraining.Logic
{
    /// <summary>
    /// Evaluates a string of characters against an expected one.
    /// </summary>
    public class InputEvaluator
    {
        /// <summary>
        /// Fired when the input's length matches the expected string's length.
        /// </summary>
        public event EventHandler<InputEvaluatorEventArgs> InputComplete;

        /// <summary>
        /// The input currently in the evaluator.
        /// </summary>
        public string CurrentInput { get; private set; }
        public string ExpectedInput { get; private set; }
        
        bool acceptInput;

        /// <summary>
        /// Sets up the evaluator to start accepting input.
        /// </summary>
        /// <param name="expected">The expected input.</param>
        public void SetExpectedInput(string expected)
        {
            acceptInput = true;
            ExpectedInput = expected;
            CurrentInput = "";
        }

        /// <summary>
        /// Adds a character to the input buffer.
        /// </summary>
        /// <param name="c">Character to add.</param>
        public void AddInput(char c)
        {
            if (!acceptInput) return;
            CurrentInput += c;
            if (ExpectedInput.Length == CurrentInput.Length)
            {
                acceptInput = false;
                OnInputComplete(CurrentInput, ExpectedInput, CurrentInput == ExpectedInput);
            }
        }

        void OnInputComplete(string enteredInput, string expectedInput, bool success)
        {
            InputComplete?.Invoke(this, new InputEvaluatorEventArgs(enteredInput, expectedInput, success));
        }
    }
}
