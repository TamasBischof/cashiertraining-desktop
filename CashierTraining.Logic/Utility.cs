﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashierTraining.Logic
{
    public static class Utility
    {
        /// <summary>
        /// Randomly shuffles an IList collection.
        /// </summary>
        /// <param name="collection">Collection to shuffle.</param>
        public static void Shuffle<T>(this IList<T> collection)
        {
            var rand = new Random();

            int n = collection.Count;
            while (n > 1)
            {
                n--;
                int k = rand.Next(n + 1);
                T value = collection[k];
                collection[k] = collection[n];
                collection[n] = value;
            }
        }
    }
}
