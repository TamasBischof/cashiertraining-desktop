﻿using System;

namespace CashierTraining.Logic
{
    /// <summary>
    /// EventArgs implementation for InputEvaluator.InputComplete.
    /// </summary>
    public class InputEvaluatorEventArgs : EventArgs
    {
        public string EnteredInput;
        public string ExpectedInput;
        public bool Success;

        public InputEvaluatorEventArgs(string enteredInput, string expectedInput, bool success)
        {
            EnteredInput = enteredInput;
            ExpectedInput = expectedInput;
            Success = success;
        }
    }
}