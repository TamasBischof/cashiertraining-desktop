﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashierTraining.Logic
{
    /// <summary>
    /// Keeps track of the user's score so far.
    /// </summary>
    public class ScoreTracker
    {
        public string Score => $"{currentScore} / {maxScore}";
        public string ScoreOnSecond => $"{scoreOnSecond}";
        public string Failed => $"{failed}";
        int maxScore;
        int currentScore;
        int scoreOnSecond;
        int failed;

        public ScoreTracker(int maxScore)
        {
            this.maxScore = maxScore;
        }

        public void Add()
        {
            currentScore++;
        }

        public void AddRetrySuccess()
        {
            scoreOnSecond++;
        }

        public void AddFailed()
        {
            failed++;
        }
    }
}
