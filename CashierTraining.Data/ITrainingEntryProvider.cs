﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CashierTraining.Data
{
    /// <summary>
    /// Interface to provide CashierTrainingEntries.
    /// </summary>
    public interface ITrainingEntryProvider
    {
        /// <summary>
        /// Should return a collection of entries.
        /// </summary>
        IEnumerable<TrainingEntry> GetEntries();
        /// <summary>
        /// Display name to use on the UI.
        /// </summary>
        string DisplayName { get; }
        /// <summary>
        /// Whether the user has selected this provider.
        /// </summary>
        bool Selected { get; }
    }
}
