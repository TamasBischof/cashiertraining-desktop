﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CashierTraining.Data
{
    /// <summary>
    /// Collection of ChashierTrainingEntries, parsed from a .csv file.
    /// </summary>
    public class ParsedCSVTrainingData : ITrainingEntryProvider
    {
        public bool Selected { get; set; }
        public string DisplayName { get; private set; }
        public List<TrainingEntry> Entries { get; private set; }
        public bool Valid { get; private set; }

        /// <summary>
        /// Creates a new instance of this class by parsing a .csv file.
        /// </summary>
        /// <param name="path">Absolute path of the file to parse.</param>
        public ParsedCSVTrainingData(string path)
        {
            Entries = new List<TrainingEntry>();
            Selected = true;
            DisplayName = Path.GetFileName(path);
            Valid = true;

            var lines = File.ReadAllLines(path, Encoding.Default);

            foreach (var line in lines)
            {
                string[] values = line.Split(',');
                if (values.Length != 2)
                {
                    Valid = false;
                    return;
                }
                try
                {
                    Entries.Add(new TrainingEntry(int.Parse(values[0]), values[1]));
                }
                catch
                {
                    Valid = false;
                    return;
                }
            }
        }

        public override string ToString()
        {
            return DisplayName;
        }

        public IEnumerable<TrainingEntry> GetEntries()
        {
            return Entries;
        }
    }
}
