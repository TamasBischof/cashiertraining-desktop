﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CashierTraining.Data
{
    public class EntryProviderFactory
    {
        public List<ITrainingEntryProvider> GetProviders()
        {
            var results = new List<ITrainingEntryProvider>();

            foreach (var file in Directory.GetFiles(Path.GetFullPath(@"CSV"), $"*.csv"))
            {
                var csv = new ParsedCSVTrainingData(file);
                if (csv.Valid)
                {
                    results.Add(csv);
                }
            }

            //if later different kinds are added...

            return results;
        }

        public ITrainingEntryProvider GetCSVProvider(string fullPath)
        {
            var csv = new ParsedCSVTrainingData(fullPath);
            if (csv.Valid)
            {
                return csv;
            }
            return null;
        }
    }
}
