﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CashierTraining.Data
{
    public enum Status { Pending, Retry, Success, Failed }

    /// <summary>
    /// Represents a product code - product name pair.
    /// </summary>
    public struct TrainingEntry : INotifyPropertyChanged
    {
        /// <summary>
        /// Null object implementation.
        /// </summary>
        public static TrainingEntry Null { get { return nullObj; } }
        static TrainingEntry nullObj = new TrainingEntry(0, "");

        public TrainingEntry(int productCode, string productName) : this()
        {
            ProductCode = productCode.ToString();
            ProductName = productName;
            CurrentStatus = Status.Pending;
        }

        public string ProductCode { get; }
        public string ProductName { get; }
        Status currentStatus;
        public Status CurrentStatus
        {
            get
            {
                return currentStatus;
            }
            set
            {
                var name = ProductName;
                currentStatus = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string propName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        public override bool Equals(object obj)
        {
            if (!(obj is TrainingEntry))
            {
                return false;
            }

            var entry = (TrainingEntry)obj;
            return ProductCode == entry.ProductCode &&
                   ProductName == entry.ProductName;
        }

        public override int GetHashCode()
        {
            var hashCode = -176322344;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ProductCode);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(ProductName);
            return hashCode;
        }

        public static bool operator ==(TrainingEntry a, TrainingEntry b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(TrainingEntry a, TrainingEntry b)
        {
            return !a.Equals(b);
        }
    }
}