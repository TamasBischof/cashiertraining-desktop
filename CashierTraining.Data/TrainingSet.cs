﻿using CashierTraining.Logic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace CashierTraining.Data
{
    /// <summary>
    /// Data provider class, serves up TrainingEntry elements.
    /// </summary>
    public class TrainingSet : INotifyPropertyChanged, IEnumerable<TrainingEntry>
    {
        ObservableQueue<TrainingEntry> data = new ObservableQueue<TrainingEntry>();
        public ObservableQueue<TrainingEntry> Remaining => data;

        public int Count => data.Count;

        public event PropertyChangedEventHandler PropertyChanged;

        public TrainingEntry Current
        {
            get
            {
                if (data.Count != 0)
                {
                    return data.Peek();
                }
                else
                {
                    return TrainingEntry.Null;
                }
            }
        }

        public TrainingSet(IEnumerable<ITrainingEntryProvider> entryProviders, bool shuffle = true)
        {
            var entries = entryProviders.SelectMany(x => x.GetEntries()).ToList();

            if (shuffle) entries.Shuffle();

            foreach (var entry in entries)
            {
                data.Enqueue(new TrainingEntry(int.Parse(entry.ProductCode), entry.ProductName));
            }
        }

        /// <summary>
        /// Advances the dataset to the next entry.
        /// </summary>
        public void AdvanceTraining(bool successOnCurrent, ScoreTracker tracker = null)
        {
            if (data.Count > 0)
            {
                var current = data.Dequeue();
                switch (current.CurrentStatus)
                {
                    case Status.Pending:
                        if (successOnCurrent)
                        {
                            current.CurrentStatus = Status.Success;
                            tracker?.Add();
                        }
                        else
                        {
                            current.CurrentStatus = Status.Retry;
                            data.Enqueue(current);
                        }
                        break;
                    case Status.Retry:
                        if (successOnCurrent)
                        {
                            current.CurrentStatus = Status.Success;
                            tracker?.AddRetrySuccess();
                        }
                        else
                        {
                            current.CurrentStatus = Status.Failed;
                            tracker?.AddFailed();
                        }
                        break;
                    case Status.Success:
                    case Status.Failed:
                        throw new InvalidOperationException("Successful/failed entries should not be on the queue!");
                }
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Current)));
            }
        }

        public void AdvanceLearning(bool successOnCurrent, ScoreTracker tracker = null)
        {
            if (data.Count > 0)
            {
                var current = data.Dequeue();
                switch (current.CurrentStatus)
                {
                    case Status.Pending:
                        if (successOnCurrent)
                        {
                            current.CurrentStatus = Status.Retry;
                        }
                        else
                        {
                            current.CurrentStatus = Status.Pending;
                        }
                        data.Enqueue(current);
                        break;
                    case Status.Retry:
                        if (successOnCurrent)
                        {
                            current.CurrentStatus = Status.Success;
                            tracker?.Add();
                        }
                        else
                        {
                            data.Enqueue(current);
                            tracker?.AddFailed();
                        }
                        break;
                    case Status.Success:
                    case Status.Failed:
                        throw new InvalidOperationException("Successful/failed entries should not be on the queue!");
                }
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Current)));
            }
        }

        public IEnumerator<TrainingEntry> GetEnumerator()
        {
            return data.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
